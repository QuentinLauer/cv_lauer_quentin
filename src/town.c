#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "town.h"
#include "road.h"

struct town * createTown(char * name) {
	struct town * T = (struct town *) calloc(1, sizeof(struct town));
	T->name = name;
	struct list * alist = new ();
	T->alist = alist;
	return T;
}

void freeTown ( struct town * T ) {
	free(T);
}

char * getTownName(struct town * T) {
	return T->name;
}

struct list * getAList(struct town * T) {
	return T->alist;
}

void viewTown (struct town * T) {
  if(T == NULL) {
    printf("[ ] //empty list\n");
  }
  else {
	printf("\t %s connected to \n", T->name);
    
    struct elmlist * iterator = T->alist->head;

    while(iterator) {
		struct road * road = iterator->data;
		if (strcmp(T->name, road->U->name) == 0)
			printf("\t\t %s ", road->V->name);
		else
			printf("\t\t %s ", road->U->name);
			
		printf("via ( %s , %s )\n", road->U->name, road->V->name);
		iterator = iterator->suc;
    }
	puts("");
  }
}

void printTownName ( struct town * T ) {
	printf("\t Town name: %s", T->name);
}