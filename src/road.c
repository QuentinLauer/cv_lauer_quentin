#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "road.h"
#include "town.h"

struct road * createRoad (struct town * U, struct town * V) {
	struct road * R = (struct road *) calloc(1, sizeof(struct road));
	R->U = U;
	R->V = V;
	//R->km = km;
	return R;
}

void freeRoad ( struct road * R ) {
	assert(R != NULL);
	printf("freeing road");
	free(R);
}

struct town * getURoad(struct road * R) {
	return R->U;
}

void setURoad ( struct road * R, struct town * T ) {
	R->U = T;
}

struct town * getVRoad(struct road * R) {
	return R->V;
}

void setVRoad ( struct road * R, struct town * T ) {
	R->V = T;
}

/*
double getDistance(struct road * R){
	return R->km;
}

void setDistance ( struct road * R, double km ){
	R->km = km;
}
*/

void viewRoad ( struct road * R ) {
	assert(R != NULL);
	//printf("This road is %lf km long and connects %s and %s", R->km, R->U->name, R->V->name);
	printf("This road connects %s and %s\n", R->U->name, R->V->name);
}