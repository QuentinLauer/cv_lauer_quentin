#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"
#include "town.h"
#include "road.h"

struct list * new () {
    struct list * L = (struct list *) calloc(1, sizeof(struct list));
    assert(L != NULL);
    L->tail = NULL;
    L->head = NULL;
    return L;
}

bool isempty ( struct list * L ) {
    assert(L != NULL);
    return L->numelm == 0;

}

void dellist ( struct list * L, void (*ptrF) () ) {
  struct elmlist * iterator = L->head;

  if ( ptrF == NULL ) { // ne supprime pas les données
        free(L);
  } else { // suppression complète

    for(iterator = L->head; iterator; ) {
        (*ptrF)(iterator);
    }
    free(L);
  }
  
}

void viewlist ( struct list * L, void (*ptrF) () ) {
  printf("\t\t|| View data list ||\n");
  if(isempty(L)) {
    printf("[ ] //empty list\n");
  }
  else {
    struct elmlist * iterator = L->head;
    while(iterator) {
      printf("%p \n", iterator->data);
      iterator = iterator->suc;
    }
  }

}

void cons ( struct list * L, void * data ) {
    assert(L != NULL); 

    struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
    E->data = data;
    E->suc = L->head;
    if( L->head ) {
        L->head->pred = E;
    } else {
        L->tail = E;
    }
    L->head = E;
    L->numelm += 1;

}

void insert_after(struct list * L, void * data, struct elmlist * ptrelm) {
    assert(L != NULL); 

    struct elmlist * E = (struct elmlist *) calloc( 1, sizeof(struct elmlist));
    E->data = data;
    E->pred = ptrelm;

    if ( L->tail == ptrelm ){
        L->tail = E;
    } else {
        E->suc = ptrelm->suc;
        ptrelm->suc->pred = E;
    }
    
    ptrelm->suc = E;
    L->numelm += 1;

}

void insert_ordered ( struct list * L, void * data, struct town * departure) {
 struct elmlist * iterator;
 if( L->head == NULL) {
   cons(L, data);
 } else {
 

    iterator = L->head;

    if( departure == NULL) { // C'est la liste des villes
        while (iterator != NULL){
            if(strcmp(getTownName(data), getTownName(iterator->data))>0)
                iterator = iterator->suc;
            else 
                break;
        }
    } else { // C'est une liste d'ajacence, une liste de routes
        if (strcmp(getTownName(getURoad(data)), getTownName(departure)) == 0){
            while (iterator != NULL){
                if (strcmp(getTownName(getURoad(iterator->data)), getTownName(departure)) == 0){
                    if (strcmp(getTownName(getVRoad(data)), getTownName(getVRoad(iterator->data)))>0)
                        iterator = iterator->suc;
                    else 
                        break;
                } else{
                    if (strcmp(getTownName(getVRoad(data)), getTownName(getURoad(iterator->data)))>0)
                        iterator = iterator->suc;
                    else 
                        break;
                }
            }
        }
        else {
            while (iterator != NULL){
                if (strcmp(getTownName(getURoad(iterator->data)), getTownName(departure)) == 0){
                    if (strcmp(getTownName(getURoad(data)), getTownName(getVRoad(iterator->data)))>0)
                        iterator = iterator->suc;
                    else 
                        break;
                } else{
                    if (strcmp(getTownName(getURoad(data)), getTownName(getURoad(iterator->data)))>0)
                        iterator = iterator->suc;
                    else 
                        break;
                }
            }
        }
    }
 
 
    
    
    if ( iterator == NULL) { // Ajout en queue
      insert_after(L, data, L->tail);
    } else if ( iterator == L->head ) { // Ajout en Tête
      cons(L, data);
    } else {
      insert_after(L, data, iterator->pred);
    }

  }
}